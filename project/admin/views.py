from flask import render_template
from flask_login import login_required

from project.admin import admin
from project.utilities.decorators import is_admin


@admin.route("/", methods=['GET', 'POST'])
@is_admin
@login_required
def index():
    return render_template("admin/index.html")

