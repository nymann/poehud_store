from flask import Blueprint

profiles = Blueprint(
    name='profiles',
    import_name=__name__,
    url_prefix='/profiles',
    template_folder='templates'
)

from . import views