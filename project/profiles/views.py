from flask import render_template, abort

from project.models import User, Plugin, BillingAgreement
from project.profiles import profiles


@profiles.route("/<string:username>")
def index(username):
    user = User.query.get(username)
    if not user:
        return abort(404)

    developed_plugins = Plugin.query.filter(Plugin.author == username).all()
    return render_template("profiles/profile.html", user=user, developed_plugins=developed_plugins)
