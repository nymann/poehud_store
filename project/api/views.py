from datetime import datetime, timedelta

from flask import abort, url_for, request, redirect, flash, jsonify
import paypalrestsdk
from flask_babelplus import gettext
from flask_login import current_user

from project.api import api
from project.models import Plugin, BillingAgreement


# Step 3 subscribe to payment (creat billing agreement)
@api.route("subscribe/<string:plugin_name>")
def subscribe(plugin_name):
    plugin = Plugin.query.get(plugin_name)
    if not plugin:
        return abort(502)

    id = request.args.get("id", default=None, type=str)
    if not id:
        return abort(502)

    billing_agreement = paypalrestsdk.BillingAgreement({
        "name": "Organization plan name",
        "description": f"Agreement for {plugin.name}",
        # Start date must be greater than the current date.
        "start_date": (datetime.now() + timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%SZ'),
        "plan": {
            "id": id
        },
        "payer": {
            "payment_method": "paypal"
        }
    })

    if billing_agreement.create():
        approval_url = ""
        for link in billing_agreement.links:
            if link.rel == "approval_url":
                approval_url = link.href
        return redirect(approval_url)
    else:
        return f"error: {billing_agreement.error}"


@api.route("/payment/execute")
def execute():
    payment_token = request.args.get('token', default=None, type=str)
    if not payment_token:
        flash_message = gettext("No payment token provided.")
        flash(flash_message, "danger")
        return abort(502)

    plugin_name = request.args.get("plugin_name", default=None, type=str)
    if not plugin_name:
        flash_message = gettext("No plugin name provided.")
        flash(flash_message, "danger")
        return abort(502)

    plugin = Plugin.query.get(plugin_name)
    if not plugin:
        flash_message = gettext("Could not find plugin with name:")
        flash(f"{flash_message} {plugin_name}", "danger")
        return abort(502)

    billing_agreement_response = paypalrestsdk.BillingAgreement.execute(payment_token)
    ba = BillingAgreement(id=billing_agreement_response.id, plugin_id=plugin.name, user_id=current_user.username)
    ba.store()
    plugin.add_subscriber(user=current_user)

    flash_message = gettext("Billing agreement confirmed")
    flash(flash_message, "success")

    return redirect(url_for("plugins.index", name=plugin_name))


@api.route("/billing_agreement/<string:id>")
def billing_agreement(id):
    billing_agreement = paypalrestsdk.BillingAgreement.find(id)
    return jsonify(billing_agreement.to_dict())


@api.route("/billing_agreement/<string:id>/cancel")
def cancel_billing_agreement(id):
    billing_agreement = paypalrestsdk.BillingAgreement.find(id)
    ba = BillingAgreement.query.get(id)
    plugin = Plugin.query.get(ba.plugin_id)

    cancel_note = {"note": "Canceling the agreement"}
    if billing_agreement.cancel(cancel_note):
        billing_agreement = paypalrestsdk.BillingAgreement.find(id)

        flash_message = "Cancelled billing, status is now:"
        plugin.remove_subscriber(current_user)
        flash(f"{flash_message} {billing_agreement.status}", category="success")
    else:
        flash_message = gettext("Couldn't cancel billing agreement:")
        print(billing_agreement.error)
        flash(f"{flash_message} {billing_agreement.error}", category="danger")

    return redirect(url_for("plugins.index", name=plugin.name))
