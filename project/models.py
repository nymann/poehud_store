from datetime import datetime

from flask import flash
from flask_babelplus import gettext
from flask_login import UserMixin, current_user
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError, SQLAlchemyError

from project.utilities.uploadsets import images, process_plugin_image
from project import bcrypt

db = SQLAlchemy()

plugins_and_subscribers = db.Table(
    'plugins_and_subscribers',
    db.metadata,
    db.Column("user_username", db.String, db.ForeignKey("user.username")),
    db.Column("plugin_name", db.String, db.ForeignKey("plugin.name"))
)


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    username = db.Column(db.String, primary_key=True)
    email = db.Column(db.String, unique=True)
    admin = db.Column(db.Boolean, default=False)
    password = db.Column(db.String, nullable=False)
    activate = db.Column(db.Boolean, default=False)
    billing_agreements = db.relationship("BillingAgreement")

    plugins = db.relationship(
        "Plugin",
        secondary=plugins_and_subscribers,
        back_populates="subscribers"
    )

    def get_id(self):
        return self.username

    def __init__(self, form):
        self.email = form.email.data.lower()
        password = form.password.data.encode('utf-8')
        self.password = bcrypt.generate_password_hash(password=password, rounds=10).decode('utf-8')
        self.username = form.username.data.lower()
        self.admin = False

    def store(self):
        try:
            db.session.add(self)
            db.session.commit()
        except IntegrityError as e:
            flash_text = gettext("A user with that email already exists.")
            flash(flash_text, "danger")
            db.session.rollback()
        except SQLAlchemyError as e:
            # This should just throw the 500 internal server error.
            db.session.rollback()


class Plugin(db.Model):
    __tablename__ = 'plugin'
    name = db.Column(db.String, primary_key=True)
    author = db.Column(db.String, db.ForeignKey("user.username"))
    description = db.Column(db.Text)
    image_path = db.Column(db.String)
    last_updated = db.Column(db.DateTime)
    version = db.Column(db.String, default="0.1.0")
    created_date = db.Column(db.DateTime, default=datetime.now())
    paid = db.Column(db.Boolean)
    billing_plans = db.relationship("BillingPlan")
    billing_agreements = db.relationship("BillingAgreement")

    subscribers = db.relationship(
        "User",
        secondary=plugins_and_subscribers,
        back_populates="plugins"
    )

    def __init__(self, form):
        self.name = form.name.data
        self.author = current_user.username
        description = form.description.data

        description = description.replace('<img src=\\', '<img src=')
        description = description.replace('\\">', '">')

        if description.startswith('"'):
            description = description[1:]

        if description.endswith('"'):
            description = description[:-1]

        self.description = description

        if form.plugin_image.data:
            self.image_path = process_plugin_image(form.plugin_image.data, images)
        if form.version.data:
            self.version = form.version.data  # Major.Minor.Update
        self.last_updated = datetime.now()

        self.paid = form.paid.data

    def store(self):
        try:
            db.session.add(self)
            db.session.commit()
        except IntegrityError as e:
            flash_text = "A plugin with that name already exists."
            flash(flash_text, "danger")
            db.session.rollback()
        except SQLAlchemyError as e:
            # This should just throw the 500 internal server error.
            db.session.rollback()

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except SQLAlchemyError as e:
            # This should just throw the 500 internal server error.
            db.session.rollback()

    def add_subscriber(self, user):
        try:
            self.subscribers.append(user)
            db.session.commit()
        except SQLAlchemyError as e:
            # This should just throw the 500 internal server error.
            db.session.rollback()

    def remove_subscriber(self, user):
        try:
            self.subscribers.remove(user)
            db.session.commit()

        except SQLAlchemyError as e:
            # This should just throw the 500 internal server error.
            db.session.rollback()


class BillingPlan(db.Model):
    __tablename__ = 'billing_plan'
    id = db.Column(db.String, primary_key=True)
    plugin_id = db.Column(db.String, db.ForeignKey("plugin.name"))

    def store(self):
        try:
            db.session.add(self)
            db.session.commit()
        except SQLAlchemyError as e:
            # This should just throw the 500 internal server error.
            db.session.rollback()


class BillingAgreement(db.Model):
    __tablename__ = "billing_agreement"
    id = db.Column(db.String, primary_key=True)
    plugin_id = db.Column(db.String, db.ForeignKey("plugin.name"))
    user_id = db.Column(db.String, db.ForeignKey("user.username"))

    def store(self):
        try:
            db.session.add(self)
            db.session.commit()
        except SQLAlchemyError as e:
            # This should just throw the 500 internal server error.
            db.session.rollback()
