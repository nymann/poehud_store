import paypalrestsdk as paypal
from flask import render_template, flash, abort, redirect, url_for
from flask_babelplus import gettext
from flask_login import login_required, current_user

from config import Config
from project import celery
from project.forms import PluginForm
from project.models import Plugin, db, BillingPlan, BillingAgreement
from project.plugins import plugins
from project.utilities.decorators import is_admin


@plugins.route('/<string:name>')
@login_required
def index(name):
    plugin = Plugin.query.get(name)
    if not plugin:
        return abort(404)

    subscribed = current_user in plugin.subscribers
    can_subscribe = not subscribed and not plugin.paid or not subscribed and len(plugin.billing_plans) > 0
    return render_template("plugins/plugin.html", plugin=plugin, subscribed=subscribed, can_subscribe=can_subscribe)


@plugins.route('/new', methods=["GET", "POST"])
@is_admin
def new():
    form = PluginForm()

    if form.validate_on_submit():
        plugin = Plugin.query.get(form.name.data)
        if plugin:
            # Plugin with that name already exists.
            e = gettext("A plugin with that name already exists!")
            flash(e, "danger")
        else:
            plugin = Plugin(form)
            plugin.store()
            if plugin.paid:
                billing_plan_values = {
                    "cancel_url": url_for("plugins.unsubscribe", name=plugin.name, _external=True),
                    "return_url": url_for("api.execute", plugin_name=plugin.name, _external=True),
                    "plugin_name": plugin.name,
                    "bp_name": form.bp_name.data,
                    "bp_description": form.bp_description.data,
                    "bp_type": form.bp_type.data,
                    "bp_setup_fee": form.bp_setup_fee.data,
                    "pd_frequency_interval": form.pd_frequency_interval.data,
                    "pd_frequency": form.pd_frequency.data,
                    "pd_cycles": form.pd_cycles.data,
                    "pd_amount": form.pd_amount.data,
                }
                # Start celery background tasks.
                create_billing_plan.delay(billing_plan_values)
            return redirect(url_for("plugins.index", name=plugin.name))
    return render_template("plugins/new.html", form=form)


@plugins.route("/subscribe/<string:name>")
@login_required
def subscribe(name):
    plugin = Plugin.query.get(name)
    if not plugin:
        return abort(502)

    # If the user is already subscribed.
    subscribed = current_user in plugin.subscribers
    if subscribed:
        flash(f"You are already subscribed to {plugin.name}!", "info")
        return redirect(url_for("plugins.index", name=name))
    if not plugin.paid:
        # user = User.query.get(current_user.username)
        plugin.subscribers.append(current_user)
        db.session.commit()
        flash(f"Subscribed successfully to {plugin.name}.", "success")
        return redirect(url_for("plugins.index", name=name))
    if len(plugin.billing_plans) > 0:
        return redirect(url_for("api.subscribe", plugin_name=name, id=plugin.billing_plans[0].id))
    return abort(502)


@plugins.route("/ubsubscribe/<string:name>")
@login_required
def unsubscribe(name):
    plugin = Plugin.query.get(name)
    if not plugin:
        return abort(502)

    # If the user is already unsubscribed.
    if current_user not in plugin.subscribers:
        flash(f"You are already unsubscribed to {plugin.name}!", "info")
        return redirect(url_for("plugins.index", name=name))
    if not plugin.paid:
        # user = User.query.get(current_user.username)
        plugin.subscribers.remove(current_user)
        db.session.commit()
        flash(f"You are no longer subscribed to {plugin.name}.", "success")
        return redirect(url_for("plugins.index", name=name))

    ba = BillingAgreement.query.filter(
        BillingAgreement.user_id == current_user.username,
        BillingAgreement.plugin_id == plugin.name
    ).first()

    if not ba:
        return abort(502)

    return redirect(url_for("api.cancel_billing_agreement", id=ba.id))


@plugins.route("/delete/<string:name>")
@is_admin
def delete(name):
    plugin = Plugin.query.get(name)
    if not plugin:
        flash_message = gettext("The plugin you are trying to delete doesn't exist.")
        flash(flash_message, category="warning")
        return abort(404)
    plugin.delete()
    return redirect(url_for("site.index"))


@celery.task
def create_billing_plan(b):
    # Configure PayPal Rest SDK
    paypal.configure({
        "mode": "sandbox",
        "client_id": Config.CLIENT_ID,
        "client_secret": Config.CLIENT_SECRET
    })
    plugin_name = b["plugin_name"]
    plugin = Plugin.query.get(plugin_name)
    if not plugin:
        return f"ERROR, Could not find plugin with name: {plugin_name}"

    billing_plan_attributes = {
        "name": b["bp_name"],
        "description": b["bp_description"],
        "type": b["bp_type"],
        "merchant_preferences": {
            "auto_bill_amount": "yes",
            "cancel_url": b["cancel_url"],
            "initial_fail_amount_action": "continue",
            "max_fail_attempts": "0",
            "return_url": b["return_url"],
            "setup_fee": {
                "currency": "USD",
                "value": b["bp_setup_fee"]
            }
        },
        "payment_definitions": [
            {
                "amount": {
                    "currency": "USD",
                    "value": b["pd_amount"]
                },
                "cycles": b["pd_cycles"],
                "frequency": b["pd_frequency"],
                "frequency_interval": b["pd_frequency_interval"],
                "name": f"Regular payments for {plugin_name}.",
                "type": "REGULAR"
            }
        ]
    }

    billing_plan = paypal.BillingPlan(billing_plan_attributes)
    if billing_plan.create():
        bp = BillingPlan(id=billing_plan.id, plugin_id=plugin.name)
        bp.store()
        if billing_plan.activate():
            return f"ACTIVATED: {bp.id}"
        else:
            return f"BP Created, but failed to activate: {billing_plan.error}"
    else:
        return f"ERROR: {billing_plan.error}"
