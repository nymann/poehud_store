from flask import Blueprint

plugins = Blueprint(
    name='plugins',
    import_name=__name__,
    url_prefix='/plugins',
    template_folder='templates'
)

from . import views