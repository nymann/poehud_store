from flask_babelplus import gettext
from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, FileField, IntegerField, FloatField
from wtforms.validators import DataRequired, Length, Email


class LoginForm(FlaskForm):
    username = StringField(label='email', validators=[DataRequired()])
    u = gettext("Password should minimum be 8 characters, and maximum 100 characters.")
    password = StringField(
        label='password',
        validators=[
            Length(min=8, max=100, message=u)
        ]
    )

    def __repr__(self):
        return f"email: {self.email.data}\npassword: {self.password.data}"


class LoginRememberMeForm(LoginForm):
    remember = BooleanField(label='remember')

    def __repr__(self):
        return super(LoginRememberMeForm, self).__repr__() + "\nremember: {self.remember.data}"


class CreateUserForm(LoginForm):
    email = StringField(label='email', validators=[DataRequired(), Email()])


class PluginForm(FlaskForm):
    name = StringField(label="name")
    description = StringField(label="description")
    plugin_image = FileField(label="plugin_image")
    version = StringField(label="version") # TODO require x.x.x (major.minor.update)
    paid = BooleanField(label="paid")
    bp_name = StringField(label="bp_name")
    bp_description = StringField(label="bp_description")
    bp_type = StringField(label="bp_type")
    bp_setup_fee = IntegerField(label="bp_setup_fee")
    pd_frequency_interval = IntegerField(label="pd_frequency_interval")
    pd_frequency = StringField(label="pd_frequency")
    pd_cycles = StringField(label="pd_cycles")
    pd_amount = FloatField(label="pd_amount")
