import paypalrestsdk
import sentry_sdk
from celery import Celery
from flask import Flask, request
from flask_babelplus import Babel, gettext
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_uploads import configure_uploads
from sentry_sdk.integrations.flask import FlaskIntegration

from config import ProductionConfig, Config
from project.utilities.encoder import CustomJSONEncoder
from project.utilities.error_handlers import register_handlers
from project.utilities.uploadsets import images

login_manager = LoginManager()
login_manager.login_view = "site.login"
babel = Babel()
bcrypt = Bcrypt()
celery = Celery(__name__, broker=Config.broker_url, backend=Config.result_backend)


def create_app(cfg=None):
    app = Flask(__name__)
    app.json_encoder = CustomJSONEncoder
    if not cfg:
        cfg = ProductionConfig
    app.config.from_object(cfg)
    initialize_extensions(app=app)
    register_blueprints(app=app)
    register_handlers(app=app)
    return app


def initialize_extensions(app):
    # Database
    from project.models import db
    db.init_app(app=app)
    with app.app_context():
        db.create_all(app=app)

    # Babel
    babel.init_app(app=app)

    @babel.localeselector
    def get_locale():
        return request.accept_languages.best_match(['en'])

    # Login
    login_manager.login_message = gettext("You need to be authenticated to visit that page.")
    login_manager.login_message_category = "info"
    login_manager.init_app(app=app)
    from project.models import User

    @login_manager.user_loader
    def user_loader(email):
        return User.query.get(email)

    bcrypt.init_app(app=app)

    # Sentry
    sentry_sdk.init(
        integrations=[FlaskIntegration()],
        release="1.0.0"
    )

    # Flask Uploads
    configure_uploads(app=app, upload_sets=images)

    # Update Celery config
    celery.conf.update(app.config)

    # paypal client id and secret
    paypalrestsdk.configure({
        "mode": "sandbox",
        "client_id": Config.CLIENT_ID,
        "client_secret": Config.CLIENT_SECRET
    })


def register_blueprints(app):
    from project.site import site
    app.register_blueprint(site)

    from project.admin import admin
    app.register_blueprint(admin)

    from project.plugins import plugins
    app.register_blueprint(plugins)

    from project.profiles import profiles
    app.register_blueprint(profiles)

    from project.api import api
    app.register_blueprint(api)
