from datetime import timedelta

from flask import render_template, redirect, url_for, flash
from flask_babelplus import gettext
from flask_login import login_required, login_user, logout_user
from sqlalchemy import desc, func

from project import bcrypt
from project.forms import LoginRememberMeForm, CreateUserForm
from project.models import User, Plugin, plugins_and_subscribers
from project.site import site


@site.route('/')
@login_required
def index():
    new_plugins = Plugin.query.order_by(desc(Plugin.created_date)).limit(5)
    latest_updated_plugins = Plugin.query.order_by(desc(Plugin.last_updated)).limit(5)

    popular_plugins = Plugin.query.join(
        plugins_and_subscribers
    ).group_by(
        Plugin.name
    ).order_by(desc(func.coalesce(func.count(plugins_and_subscribers.c.user_username), 0))).all()

    return render_template(
        'site/index.html', new_plugins=new_plugins, popular_plugins=popular_plugins,
        latest_updated_plugins=latest_updated_plugins
    )


@site.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginRememberMeForm()
    if form.validate_on_submit():
        username = form.username.data.lower()
        user = User.query.get(username)
        password = form.password.data.encode('utf-8')
        if user and bcrypt.check_password_hash(pw_hash=user.password, password=password):
            login_user(user=user, remember=form.remember.data, duration=timedelta(days=30))
            e = gettext(u"logged in successfully!")
            flash(message=e, category="success")
            return redirect(url_for("site.index"))
        else:
            e = gettext(u"User does not exist!")
            flash(message=e, category="danger")
    return render_template("site/login.html", form=form)


@site.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('site.index'))


@site.route("/create_user", methods=["GET", "POST"])
def create_user():
    form = CreateUserForm()
    if form.validate_on_submit():
        username = form.username.data.lower()
        user = User.query.get(username)
        if user:
            e = gettext(u"User already exists!")
            flash(message=e, category="danger")
        else:
            user = User(form=form)
            user.store()
            e = gettext(u"User created.")
            flash(message=e, category="success")
            return redirect(url_for("site.index"))
    return render_template("site/create_user.html", form=form)
